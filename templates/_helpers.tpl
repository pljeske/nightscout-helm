{{/*
Copyright VMware, Inc.
SPDX-License-Identifier: APACHE-2.0
*/}}

{{/*
Return the proper nightscout image name
*/}}
{{- define "nightscout.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.nightscout.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "nightscout.imagePullSecrets" -}}
{{- include "common.images.renderPullSecrets" (dict "images" (list .Values.nightscout.image .Values.mongodb.image) "context" $) -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "nightscout.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Return true if cert-manager required annotations for TLS signed certificates are set in the Ingress annotations
Ref: https://cert-manager.io/docs/usage/ingress/#supported-annotations
*/}}
{{- define "nightscout.ingress.certManagerRequest" -}}
{{ if or (hasKey . "cert-manager.io/cluster-issuer") (hasKey . "cert-manager.io/issuer") }}
    {{- true -}}
{{- end -}}
{{- end -}}

{{/*
Compile all warnings into a single message.
*/}}
{{- define "nightscout.validateValues" -}}
{{- $messages := list -}}
{{/*{{- $messages := append $messages (include "nightscout.validateValues.foo" .) -}}*/}}
{{/*{{- $messages := append $messages (include "nightscout.validateValues.bar" .) -}}*/}}
{{- $messages := without $messages "validation not implemented yet" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\nVALUES VALIDATION:\n%s" $message -}}
{{- end -}}
{{- end -}}

{{- define "nightscout.generateOrLookupSecret" -}}
{{- $secretName := .secretName -}}
{{- $context := .context -}}
{{- $secretKey := .secretKey -}}
{{- $secret := default dict (lookup "v1" "Secret" $context.Release.Namespace $secretName) -}}
{{- $secretValue := randAlphaNum 20 | b64enc }}
{{- $secretData := default dict (default dict $secret).data }}
{{- if hasKey $secretData $secretKey -}}
{{ index $secretData $secretKey }}
{{- else -}}
{{ randAlphaNum 20 | b64enc }}
{{- end -}}
{{- end -}}
